﻿using System;

namespace _07_09_2020
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.Write("Indtast dit navn: ");
            string name =Console.ReadLine();
            Console.Write("Indtast alder: ");
            int alder = int.Parse(Console.ReadLine());
            if (alder <= 12)
                Console.WriteLine("Du hedder {0} og er {1} år, du er et barn!", name, alder);
            else if (alder <= 19)
                Console.WriteLine("Du hedder {0} og er {1} år gammel, du er en teenager!", name, alder);
            else if (alder <= 25)
                Console.WriteLine("Du hedder {0} og er {1} år gammel, du er en studerende!", name, alder);
            else if (alder <= 67)
                Console.WriteLine("Du hedder {0} og er {1} år gammel, du er en voksen!", name, alder);
            else 
                Console.WriteLine("Du hedder {0} og er {1} år gammel, du er en boomer!", name, alder);
        }
    }
}
